use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;

#[derive(Debug)]
struct Box {
    l: u32,
    w: u32,
    h: u32,
}

impl Box {
    fn parse(sizes: &str) -> Box {  
        let sizes = sizes.split('x').collect::<Vec<&str>>();
        Box { l: sizes[0].parse().unwrap(), w: sizes[1].parse().unwrap(), h: sizes[2].parse().unwrap() }
    }

    fn smallest(&self) -> u32 {
        let mut sizes = vec!(self.l, self.w, self.h);
        sizes.sort();

        sizes[0] * sizes[1]
    }

    fn wraper_size(&self) -> u32 {
        (2 * self.l * self.w) + (2 * self.w * self.h) + (2 * self.h * self.l) + self.smallest()
    }
}

fn main() {
    let input = File::open("./input")
        .expect("Unable to open file");

    let _buf = BufReader::new(input);
    let mut _sum = 0;

    for item in _buf.lines() {
        let b = Box::parse(&item.unwrap());
        _sum += b.wraper_size();
    }

    println!("{}", _sum);
}