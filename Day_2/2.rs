use std::io::BufReader;
use std::io::BufRead;
use std::fs::File;

#[derive(Debug)]
struct Box {
    l: u32,
    w: u32,
    h: u32,
}

impl Box {
    fn parse(sizes: &str) -> Box {  
        let sizes = sizes.split('x').collect::<Vec<&str>>();
        Box { l: sizes[0].parse().unwrap(), w: sizes[1].parse().unwrap(), h: sizes[2].parse().unwrap() }
    }

    fn sizes_sorted(&self) -> Vec<u32> {
        let mut sizes = vec!(self.l, self.w, self.h);
        sizes.sort();

        sizes
    }

    fn ribbon_size(&self) -> u32 {
        let sizes = self.sizes_sorted();
        self.l * self.w * self.h + sizes[0] + sizes[0] + sizes[1] + sizes[1]
    } 
}

fn main() {
    let input = File::open("./input")
        .expect("Unable to open file");

    let _buf = BufReader::new(input);
    let mut _sum = 0;

    for item in _buf.lines() {
        let b = Box::parse(&item.unwrap());
        _sum += b.ribbon_size();
    }

    println!("{}", _sum);
}