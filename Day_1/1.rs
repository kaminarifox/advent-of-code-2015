use std::fs;

fn main() {
    let input = fs::read_to_string("./input")
        .expect("Something went wrong reading the file");
    
    let mut floor = 0;

    for &item in input.as_bytes().iter() {
        floor += if item == b'(' { 1 } else { -1 }
    }

    println!("{}", floor);
}