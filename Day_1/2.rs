use std::fs;

fn main() {
    let input = fs::read_to_string("./input")
        .expect("Something went wrong reading the file");
    
    let mut floor = 0;

    for (_i, &item) in input.as_bytes().iter().enumerate() {
        floor += if item == b'(' { 1 } else { -1 };

        if floor == -1 {
            println!("{}", _i + 1);
            break;
        }
    }
}